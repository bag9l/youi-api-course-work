package com.example.youi.controller;

import com.example.youi.model.Post;
import com.example.youi.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("posts")
public class PostController {

    private final PostService postService;


    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }


    @GetMapping("{postId}")
    public Post getUserPost(@PathVariable("postId") String postId) {
        return postService.findPost(postId);
    }

    @GetMapping
    public List<Post> getPosts(){
        return postService.findAllPosts();
    }

    @PostMapping("create")
    public void createPost(@RequestBody Post post){
        postService.addPost(post);
    }
}
