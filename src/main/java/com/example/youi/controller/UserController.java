package com.example.youi.controller;

import com.example.youi.model.Post;
import com.example.youi.model.User;
import com.example.youi.service.UserService;
import org.apache.catalina.LifecycleState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }


//    @GetMapping("{id}")
//    public User getUser(@PathVariable String id){
//        return userService.findUserById(id);
//    }

    @GetMapping("{id}")
    public ResponseEntity<User> getUser(@PathVariable String id){
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userService.findUserById(id));
    }

    @GetMapping("{userId}/following")
    public List<User> getUserFollowing(@PathVariable("userId") String userId){
        return userService.findFollowingByUserId(userId);
    }

    @GetMapping("{userId}/followers")
    public List<User> getUserFollowers(@PathVariable("userId") String userId){
        return userService.findFollowersByUserId(userId);
    }

//    @PostMapping("/login")
//    public ResponseEntity<User> getUser(@RequestBody User user){
//        userService.fi
//
//
//        return ResponseEntity
//                .status(HttpStatus.OK)
//                .body(userService.findUserById(id));
//    }
}
