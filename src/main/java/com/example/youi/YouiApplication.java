package com.example.youi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//@EnableJpaRepositories("es.uc3m.tiw.dominios")
@SpringBootApplication()
public class YouiApplication {

    public static void main(String[] args) {
        SpringApplication.run(YouiApplication.class, args);
    }

}
