package com.example.youi.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "`post`")
public class Post {

    @Id
    @Column(name="`id`")
    private String id;

    @Column(name="`image`")
    private String image;

    @Column(name="`description`")
    private String description;

    @Column(name = "`creation date`")
    private LocalDate creationDate;

    @Column(name="`owner`")
    private String owner;

    @Column(name="`saves`")
    private String saves;

    @OneToMany(targetEntity = Comment.class, fetch = FetchType.EAGER, mappedBy = "post", cascade = {
            CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH
    })
    private List<Comment> comments;

}
