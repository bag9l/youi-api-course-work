package com.example.youi.model;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "`usr`")
public class User implements UserDetails {
    @Id
    @Column(nullable = false, updatable = false)
    private String nickname;

    @Column(name = "`password`")
    private String password;

    @Column(name = "`registration date`", updatable = false)
    private LocalDate registrationDate;

    @Column(nullable = true)
    private String email;

    @Column(nullable = true, updatable = true)
    private String savedPosts;

    private Boolean locked;

    private Boolean enabled;

    @ManyToOne
    private User parent;

    @OneToMany(mappedBy = "parent")
    private List<User> following;

    @OneToMany(mappedBy = "parent")
    private List<User> followers;

    public User(String nickname, String password, LocalDate registrationDate, String email) {
        this.nickname = nickname;
        this.password = password;
        this.registrationDate = registrationDate;
        this.email = email;
        this.locked = false;
        this.enabled = true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getUsername() {
        return nickname;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !locked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
