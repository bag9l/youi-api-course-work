package com.example.youi.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "`comment`")
public class Comment {

    @Id
    @Column(name="`id`")
    private String id;

    private String commentOwner;

    @Column(name="`text`")
    private String text;

    @Column(name = "`creation date`")
    private LocalDate creationDate;

    @ManyToOne(cascade = {
            CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH
    })
    @JoinColumn(name = "post_id")
    private Post post;
}
