package com.example.youi.repository;

import com.example.youi.model.Post;
import com.example.youi.model.User;
import jakarta.persistence.OrderColumn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface PostRepository extends JpaRepository<Post, String> {
    Optional<Post> findPostById(String id);

    Boolean deleteCommentById(String id);

//    @Query("SELECT * FROM Post WHERE owner.nickname = :ownerNickname ")
//    List<Post> findPostsByOwnerNickname(@Param("ownerNickname") String ownerNickname);
    List<Post> findPostsByOwner(User owner);

    @OrderColumn(name = "creationDate")
    List<Post> findPostsByOwnerOrderByCreationDate(User owner);
}
