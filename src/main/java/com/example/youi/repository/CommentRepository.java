package com.example.youi.repository;

import com.example.youi.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CommentRepository extends JpaRepository<Comment, String> {

    Boolean deleteCommentById(String id);

    Optional<Comment> findCommentById(String id);

    List<Comment> findCommentsByPostIdOrderByCreationDate(String postId);
}
