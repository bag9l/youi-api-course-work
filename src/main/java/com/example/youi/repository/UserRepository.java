package com.example.youi.repository;

import com.example.youi.model.Post;
import com.example.youi.model.User;
import com.example.youi.registration.UserRegistrationDTO;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional (readOnly = true )
public interface UserRepository extends JpaRepository<User, String> {
    Optional<User> findUserByNickname(String nickname);

    Optional<User> findUserByEmail(String email);

    Boolean deleteUserByNickname(String nickname);

    @Override
    <S extends User> boolean exists(Example<S> example);

    Boolean existsByNickname(String nickname);

    Optional<User> findByNicknameAndPassword(String nickname, String password);

    List<User> findFollowingByNickname(String nickname);

    List<User> findFollowersByNickname(String nickname);


//    List<Post> findPostsByUserNickname(String nickname);
}
