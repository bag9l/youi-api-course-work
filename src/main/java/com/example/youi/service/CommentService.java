package com.example.youi.service;

import com.example.youi.exceptions.NotFoundException;
import com.example.youi.model.Comment;
import com.example.youi.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class CommentService {

    private final CommentRepository commentRepository;

    @Autowired
    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    public Comment addComment (Comment comment){

        comment.setCreationDate(LocalDate.now());

        return commentRepository.save(comment);
    }

    public Comment updateComment(Comment comment){
        return commentRepository.save(comment);
    }

    public Comment findComment(String id){
        return commentRepository.findCommentById(id).orElseThrow(NotFoundException::new);
    }

    public Boolean deleteComment(String id){
        return commentRepository.deleteCommentById(id);
    }

    public List<Comment> findPostCommentsSortByCreationDate(String postId){
       return commentRepository.findCommentsByPostIdOrderByCreationDate(postId);
    }







}
