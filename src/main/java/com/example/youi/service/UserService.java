package com.example.youi.service;

import com.example.youi.exceptions.NotFoundException;
import com.example.youi.model.Post;
import com.example.youi.model.User;
import com.example.youi.registration.UserRegistrationDTO;
import com.example.youi.repository.PostRepository;
import com.example.youi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final PostRepository postRepository;

    @Autowired
    public UserService(UserRepository userRepository, PostRepository postRepository) {
        this.userRepository = userRepository;
        this.postRepository = postRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return userRepository.findUserByEmail(email)
                .orElseThrow(NotFoundException::new);
    }

    public User register(UserRegistrationDTO registrationDTO){
        User user = new User(
                registrationDTO.getNickname(),
                registrationDTO.getPassword(),
                LocalDate.now(),
                registrationDTO.getEmail()
        );

        return registerUser(user);
    }


    public User updateUser(User user){
        return userRepository.save(user);
    }

    public List<User> findAllUsers(){
        return userRepository.findAll();
    }

    public User findUserById(String id){
        return userRepository.findUserByNickname(id).orElseThrow(NotFoundException::new);
    }

    public Boolean deleteUser(String id){
        return userRepository.deleteUserByNickname(id);
    }

    private User registerUser(User user){

        boolean exists = userRepository.existsByNickname(user.getNickname());

        if(exists){
            user.setRegistrationDate(LocalDate.now());
            return userRepository.save(user);
        }else {
            throw new NotFoundException();
        }
    }

    public User loginUser(String login, String password){
        return userRepository.findByNicknameAndPassword(login,password).orElseThrow(NotFoundException::new);
    }

    public List<Post> findUserPosts(String userId){
       return postRepository.findPostsByOwner(userRepository.findUserByNickname(userId).orElseThrow(NotFoundException::new));
    }

    public List<User> findFollowingByUserId(String userId){
        return userRepository.findFollowingByNickname(userId);
    }


    public List<User> findFollowersByUserId(String userId){
        return userRepository.findFollowersByNickname(userId);
    }
}
