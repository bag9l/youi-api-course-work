package com.example.youi.service;

import com.example.youi.exceptions.NotFoundException;
import com.example.youi.model.Post;
import com.example.youi.repository.PostRepository;
import com.example.youi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class PostService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;

    @Autowired
    public PostService(PostRepository postRepository, UserRepository userRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
    }

    public Post addPost(Post post){

        post.setCreationDate(LocalDate.now());

        return postRepository.save(post);
    }

    public Post updatePost(Post post){
        return postRepository.save(post);
    }

    public Post findPost (String id){
        return postRepository.findPostById(id).orElseThrow(NotFoundException::new);
    }

    public Boolean deletePost(String id){
        return postRepository.deleteCommentById(id);
    }

    public List<Post> findPostsForUser(String userId){
        return postRepository.findPostsByOwner(userRepository.findUserByNickname(userId).orElseThrow(NotFoundException::new));
    }

    public List<Post> findAllPosts(){
        return postRepository.findAll();
    }

    public List<Post> sortPostsByCreationDate(){
        return postRepository.findAll();
    }

    public List<Post> sortUserPostsByCreationDate(String userId){
        return postRepository.findPostsByOwnerOrderByCreationDate(userRepository.findUserByNickname(userId).orElseThrow(NotFoundException::new));
    }
}
