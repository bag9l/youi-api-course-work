package com.example.youi.registration;

import com.example.youi.model.User;
import com.example.youi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("registration")
public class RegistrationController {

    private final UserService userService;

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public User registerUser(@RequestBody UserRegistrationDTO userRegistrationDTO){
        return userService.register(userRegistrationDTO);
    }
//
//
////    public String register(@RequestBody UserRegistrationDTO request){
////        return registrationService.register(request);
////    }
//
//
//    @PostMapping
//    public String registerUser(@ModelAttribute("user") UserRegistrationDTO registrationDTO){
//        userService.save(registrationDTO);
//        return "redirect:/registration?success";
//    }
}







